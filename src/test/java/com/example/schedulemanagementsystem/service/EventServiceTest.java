package com.example.schedulemanagementsystem.service;

import com.example.schedulemanagementsystem.entity.Event;
import com.example.schedulemanagementsystem.entity.RRule;
import com.example.schedulemanagementsystem.entity.User;
import com.example.schedulemanagementsystem.exception.RecordNotFoundException;
import com.example.schedulemanagementsystem.exception.WrongCreateEventMethodException;
import com.example.schedulemanagementsystem.repository.EventRepository;
import com.example.schedulemanagementsystem.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.schedulemanagementsystem.DataHelper.buildUser;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EventServiceTest {
    @Mock
    private UserService userService;
    @Mock
    private EventRepository eventRepository;
    @Mock
    private UserRepository userRepository;
    @InjectMocks
    private EventService eventService;

    @Test
    void shouldFindEventsByUserId() {
        // given
        User user = buildUser();
        List<Event> events = List.of(buildWeeklyEvent(), buildDailyCustomEvent());

        // when
        when(eventRepository.findByUserId(user.getId())).thenReturn(events);
        List<Event> actualEvents = eventService.findByUserIdProcessed(user.getId());

        // then
        assertEquals(4, actualEvents.size());
    }

    @Test
    void shouldNotCreateEvent() {
        // given
        Event event = buildDailyCustomEvent();

        // then
        assertThrows(WrongCreateEventMethodException.class, () -> eventService.createEvent(123L, event));
    }

    @Test
    void shouldCreateCustomDaysEvent() {
        // given
        Event event = buildDailyCustomEvent();
        User user = buildUser();
        user.setEvents(new ArrayList<>());

        // when
        when(userService.findById(user.getId())).thenReturn(user);
        when(eventRepository.save(event)).thenReturn(event);

        // then
        assertDoesNotThrow(() -> eventService.createCustomDaysEvents(user.getId(), event));
        assertEquals(1, user.getEvents().size());
    }

    @Test
    void shouldCreateEvent() {
        // given
        Event event = buildWeeklyEvent();
        User user = buildUser();
        user.setEvents(new ArrayList<>());

        // when
        when(userService.findById(user.getId())).thenReturn(user);

        // then
        assertDoesNotThrow(() -> eventService.createEvent(user.getId(), event));
        assertEquals(1, user.getEvents().size());
    }

    @Test
    void shouldNotFindEvent() {
        assertThrows(RecordNotFoundException.class, () -> eventService.findById(1L));
    }

    @Test
    void shouldNotThrowErrorWhileUpdatingEvent() {
        // given
        Event event1 = buildDailyCustomEvent();
        Event event2 = buildWeeklyEvent();

        // when
        when(eventRepository.findById(event1.getId())).thenReturn(Optional.of(event1));

        // then
        assertDoesNotThrow(() -> eventService.updateEvent(event1.getId(), event2));
    }

    private Event buildWeeklyEvent() {
        Event event = new Event();
        event.setDailyCustomFrequency(false);
        return event;
    }

    private Event buildDailyCustomEvent() {
        Event event = new Event();
        RRule rRule = new RRule();
        ZonedDateTime dateStart = ZonedDateTime.now();
        ZonedDateTime dateUntil = dateStart.plusDays(10);
        rRule.setInterval(5);
        rRule.setDateStart(dateStart);
        rRule.setDateUntil(dateUntil);

        event.setDailyCustomFrequency(true);
        event.setCustomDays(List.of(true, false, true, false, true));
        event.setRRule(rRule);
        return event;
    }
}