package com.example.schedulemanagementsystem.service;

import com.example.schedulemanagementsystem.entity.User;
import com.example.schedulemanagementsystem.exception.BadCredentialsException;
import com.example.schedulemanagementsystem.exception.RecordNotFoundException;
import com.example.schedulemanagementsystem.exception.UsernameTakenException;
import com.example.schedulemanagementsystem.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.example.schedulemanagementsystem.DataHelper.USER_PASSWORD;
import static com.example.schedulemanagementsystem.DataHelper.buildUser;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {
    @Mock
    private UserRepository userRepository;
    @InjectMocks
    private UserService userService;

    @Test
    void shouldThrowUsernameTakenException() {
        when(userRepository.existsByUsername(any())).thenReturn(true);
        assertThrows(UsernameTakenException.class, () -> userService.save(buildUser()));
    }

    @Test
    void shouldNotThrowUsernameTakenException() {
        when(userRepository.existsByUsername(any())).thenReturn(false);
        assertDoesNotThrow(() -> userService.save(buildUser()));
    }

    @Test
    void shouldThrowBadCredentialsException() {
        // given
        User userInDatabase = buildUser();
        User user = buildUser();
        user.setPassword("any other password");

        // when
        when(userRepository.findByUsername(userInDatabase.getUsername())).thenReturn(Optional.of(userInDatabase));

        // then
        assertThrows(BadCredentialsException.class, () -> userService.login(user));
    }

    @Test
    void shouldNotThrowBadCredentialsException() {
        // given
        User userInDatabase = buildUser();
        User user = buildUser();
        user.setPassword(USER_PASSWORD);

        // when
        when(userRepository.findByUsername(userInDatabase.getUsername())).thenReturn(Optional.of(userInDatabase));

        // then
        assertDoesNotThrow(() -> userService.login(user));
    }

    @Test
    void shouldThrowRecordNotFoundException() {
        // given
        User user = buildUser();

        // then
        assertThrows(RecordNotFoundException.class, () -> userService.findById(user.getId()));
    }

    @Test
    void shouldNotThrowRecordNotFoundException() {
        // given
        User user = buildUser();

        // when
        when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));

        // then
        assertDoesNotThrow(() -> userService.findById(user.getId()));
    }

    @Test
    void shouldPassValidationWhileUpdatingUserWithSameUsername() {
        // given
        User user = buildUser();
        user.setUsername("newUsername");
        User userInDatabase = buildUser();

        // when
        when(userRepository.findById(1L)).thenReturn(Optional.of(userInDatabase));

        // then
        assertDoesNotThrow(() -> userService.update(1L, user));
    }
}