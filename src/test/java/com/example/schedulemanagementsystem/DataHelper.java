package com.example.schedulemanagementsystem;

import com.example.schedulemanagementsystem.entity.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class DataHelper {
    public final static String USER_PASSWORD = "password";

    public static User buildUser() {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        User user = new User();
        user.setId(1L);
        user.setUsername("username");
        user.setPassword(passwordEncoder.encode(USER_PASSWORD));
        user.setEmail("email@test.com");
        return user;
    }
}
