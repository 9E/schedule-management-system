package com.example.schedulemanagementsystem.integration;

import com.example.schedulemanagementsystem.entity.User;
import com.example.schedulemanagementsystem.service.UserService;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;

import static com.example.schedulemanagementsystem.DataHelper.buildUser;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ActiveProfiles("test")
@Disabled
public class UserServiceIntegrationTest {

    private final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    @Autowired
    private UserService userService;

    @Test
    void shouldCreateUserWithEncodedPassword() {
        // given
        User user = buildUser();
        String password = "some password";
        user.setPassword(password);

        // when
        User createdUser = userService.save(user);
        User foundCreatedUser = userService.findById(createdUser.getId());

        // then
        assertTrue(passwordEncoder.matches(password, foundCreatedUser.getPassword()));
    }
}
