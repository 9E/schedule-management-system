package com.example.schedulemanagementsystem.controller;

import com.example.schedulemanagementsystem.dto.UserRequest;
import com.example.schedulemanagementsystem.dto.UserResponse;
import com.example.schedulemanagementsystem.entity.User;
import com.example.schedulemanagementsystem.mapper.UserMapper;
import com.example.schedulemanagementsystem.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
public class UserController {
    private final UserService userService;
    private final UserMapper userMapper;

    @PostMapping
    public ResponseEntity<UserResponse> createUser(@RequestBody UserRequest userRequest) {
        User user = userMapper.userRequestToUser(userRequest);
        User createdUser = userService.save(user);
        UserResponse userResponse = userMapper.userToUserResponse(createdUser);
        return ResponseEntity.status(HttpStatus.CREATED).body(userResponse);
    }

    @PostMapping("/login")
    public ResponseEntity<UserResponse> login(@RequestBody UserRequest userRequest) {
        User user = userMapper.userRequestToUser(userRequest);
        User loggedInUser = userService.login(user);
        return ResponseEntity.ok(userMapper.userToUserResponse(loggedInUser));
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserResponse> update(@PathVariable Long id, @RequestBody UserRequest userRequest) {
        User user = userMapper.userRequestToUser(userRequest);
        User updatedUser = userService.update(id, user);
        return ResponseEntity.ok(userMapper.userToUserResponse(updatedUser));
    }
}
