package com.example.schedulemanagementsystem.controller;

import com.example.schedulemanagementsystem.dto.EventRequest;
import com.example.schedulemanagementsystem.dto.EventResponse;
import com.example.schedulemanagementsystem.entity.Event;
import com.example.schedulemanagementsystem.mapper.EventMapper;
import com.example.schedulemanagementsystem.service.EventService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
public class EventController {
    private final EventService eventService;
    private final EventMapper eventMapper;

    @GetMapping("/users/{userId}/events")
    public ResponseEntity<List<EventResponse>> getUserEvents(@PathVariable Long userId) {
        List<Event> events = eventService.findByUserIdProcessed(userId);
        return ResponseEntity.ok(eventMapper.eventsToEventResponses(events));
    }

    @GetMapping("/events/{eventId}")
    public ResponseEntity<EventResponse> getEvent(@PathVariable Long eventId) {
        Event event = eventService.findById(eventId);
        return ResponseEntity.ok(eventMapper.eventToEventDto(event));
    }

    @PostMapping("/users/{userId}/events")
    public ResponseEntity<EventResponse> createEvent(@PathVariable Long userId,
                                                     @RequestBody EventRequest eventRequest) {
        Event event = eventMapper.eventRequestToEvent(eventRequest);
        Event createdEvent = eventService.createEvent(userId, event);
        EventResponse eventResponse = eventMapper.eventToEventDto(createdEvent);
        return ResponseEntity.status(HttpStatus.CREATED).body(eventResponse);
    }

    @PostMapping("/users/{userId}/events/customDays")
    public ResponseEntity<List<EventResponse>> createEventsWithCustomDays(@PathVariable Long userId,
                                                     @RequestBody EventRequest eventRequest) {
        Event event = eventMapper.eventRequestToEvent(eventRequest);
        List<Event> createdEvents = eventService.createCustomDaysEvents(userId, event);
        List<EventResponse> eventResponses = eventMapper.eventsToEventResponses(createdEvents);
        return ResponseEntity.status(HttpStatus.CREATED).body(eventResponses);
    }

    @PutMapping("/events/{eventId}")
    public ResponseEntity<EventResponse> updateEvent(@PathVariable Long eventId,
                                                     @RequestBody EventRequest eventRequest) {
        Event event = eventMapper.eventRequestToEvent(eventRequest);
        Event updatedEvent = eventService.updateEvent(eventId, event);
        return ResponseEntity.ok(eventMapper.eventToEventDto(updatedEvent));
    }

    @DeleteMapping("/events/{eventId}")
    public ResponseEntity deleteEvent(@PathVariable Long eventId) {
        eventService.deleteById(eventId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/users/{userId}/events/unprocessed")
    public ResponseEntity<List<EventResponse>> getUnprocessedUserEvents(@PathVariable Long userId) {
        List<Event> events = eventService.findByUserId(userId);
        return ResponseEntity.ok(eventMapper.eventsToEventResponses(events));
    }

    @GetMapping("/events/{eventId}/processed")
    public ResponseEntity<List<EventResponse>> getProcessedEvents(@PathVariable Long eventId) {
        List<Event> event = eventService.findByIdProcessed(eventId);
        return ResponseEntity.ok(eventMapper.eventsToEventResponses(event));
    }
}
