package com.example.schedulemanagementsystem.mapper;

import com.example.schedulemanagementsystem.dto.EventRequest;
import com.example.schedulemanagementsystem.dto.EventResponse;
import com.example.schedulemanagementsystem.entity.Event;
import com.example.schedulemanagementsystem.entity.Frequency;
import com.example.schedulemanagementsystem.entity.Weekday;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Mapper(componentModel = "spring")
public interface EventMapper {

    List<EventResponse> eventsToEventResponses(List<Event> events);

    @Mapping(source = "rrule", target = "RRule")
    @Mapping(source = "rrule.freq", target = "RRule.frequency", qualifiedByName = "numberToFrequency")
    @Mapping(source = "rrule.dtstart", target = "RRule.dateStart")
    @Mapping(source = "rrule.until", target = "RRule.dateUntil")
    @Mapping(source = "rrule.byweekday", target = "RRule.weekdays", qualifiedByName = "numbersToWeekdays")
    Event eventRequestToEvent(EventRequest eventRequest);

    @Mapping(source = "RRule", target = "rrule")
    @Mapping(source = "RRule.frequency", target = "rrule.freq", qualifiedByName = "frequencyToNumber")
    @Mapping(source = "RRule.dateStart", target = "rrule.dtstart")
    @Mapping(source = "RRule.dateUntil", target = "rrule.until")
    @Mapping(source = "RRule.weekdays", target = "rrule.byweekday", qualifiedByName = "weekdaysToNumbers")
    EventResponse eventToEventDto(Event event);

    @Named("numberToFrequency")
    default Frequency numberToFrequency(Integer number) {
        return Optional.ofNullable(number)
                .map(Frequency::fromInt)
                .orElse(null);
    }

    @Named("frequencyToNumber")
    default Integer frequencyToNumber(Frequency frequency) {
        return Optional.ofNullable(frequency)
                .map(Frequency::getNumber)
                .orElse(null);
    }

    @Named("numbersToWeekdays")
    default List<Weekday> numbersToWeekdays(List<Integer> numbers) {
        return Optional.ofNullable(numbers)
                .map(theNumbers -> theNumbers.stream()
                        .map(Weekday::fromInt)
                        .toList()).orElse(null);
    }

    @Named("weekdaysToNumbers")
    default List<Integer> weekdaysToNumbers(List<Weekday> weekdays) {
        return Optional.ofNullable(weekdays)
                .map(theWeekdays -> theWeekdays.stream()
                        .map(Weekday::getNumber)
                        .toList()).orElse(null);
    }
}
