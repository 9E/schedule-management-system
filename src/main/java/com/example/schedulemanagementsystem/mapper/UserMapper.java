package com.example.schedulemanagementsystem.mapper;

import com.example.schedulemanagementsystem.dto.UserRequest;
import com.example.schedulemanagementsystem.dto.UserResponse;
import com.example.schedulemanagementsystem.entity.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {

    User userRequestToUser(UserRequest userRequest);

    UserResponse userToUserResponse(User user);
}
