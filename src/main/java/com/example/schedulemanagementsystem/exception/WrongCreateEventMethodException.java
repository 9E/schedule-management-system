package com.example.schedulemanagementsystem.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class WrongCreateEventMethodException extends RuntimeException {

    public WrongCreateEventMethodException(String message) {
        super(message);
    }
}
