package com.example.schedulemanagementsystem.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class IncorrectCustomDaysSizeException extends RuntimeException {

    public IncorrectCustomDaysSizeException(String message) {
        super(message);
    }
}
