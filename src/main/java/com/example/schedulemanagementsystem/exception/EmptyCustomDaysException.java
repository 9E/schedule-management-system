package com.example.schedulemanagementsystem.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class EmptyCustomDaysException extends RuntimeException {

    public EmptyCustomDaysException(String message) {
        super(message);
    }
}
