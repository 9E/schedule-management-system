package com.example.schedulemanagementsystem.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DurationDto {
    private Long days;
    private Long hours;
    private Long minutes;
}
