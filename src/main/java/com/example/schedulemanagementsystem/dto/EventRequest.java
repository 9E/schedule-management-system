package com.example.schedulemanagementsystem.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class EventRequest {
    private String title;
    private Boolean allDay;
    private RRuleDto rrule;
    private DurationDto duration;
    @JsonProperty(value = "isDailyCustomFrequency")
    private boolean isDailyCustomFrequency;
    private List<Boolean> customDays;
}
