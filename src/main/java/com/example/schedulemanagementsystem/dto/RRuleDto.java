package com.example.schedulemanagementsystem.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.ZonedDateTime;
import java.util.List;

@Getter
@Setter
public class RRuleDto {
    private Integer freq;
    private Integer interval;
    private List<Integer> byweekday;
    private ZonedDateTime dtstart;
    private ZonedDateTime until;
}
