package com.example.schedulemanagementsystem.entity;

import com.vladmihalcea.hibernate.type.array.ListArrayType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "events")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@TypeDefs(@TypeDef(name = "list-array", typeClass = ListArrayType.class))
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private Boolean allDay;
    @Embedded
    private RRule rRule;
    @ManyToOne
    private User user;
    @Embedded
    private Duration duration;
    private boolean isDailyCustomFrequency;
    @Type(type = "list-array")
    @Column(columnDefinition = "boolean[]")
    private List<Boolean> customDays;

    public Event(Event event) {
        this.id = event.id;
        this.title = event.title;
        this.allDay = event.allDay;
        this.rRule = event.rRule == null ? null : new RRule(event.rRule);
        this.user = event.user == null ? null : new User(event.user);
        this.duration = event.duration == null ? null : new Duration(event.duration);
        this.isDailyCustomFrequency = event.isDailyCustomFrequency;
        this.customDays = event.customDays.stream().toList();
    }
}
