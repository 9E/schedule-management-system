package com.example.schedulemanagementsystem.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Duration {
    private Long days;
    private Long hours;
    private Long minutes;

    public Duration(Duration duration) {
        this.days = duration.days;
        this.hours = duration.hours;
        this.minutes = duration.minutes;
    }
}
