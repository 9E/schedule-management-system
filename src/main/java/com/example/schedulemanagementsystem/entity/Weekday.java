package com.example.schedulemanagementsystem.entity;

import java.security.InvalidParameterException;

public enum Weekday {
    MONDAY(0),
    TUESDAY(1),
    WEDNESDAY(2),
    THURSDAY(3),
    FRIDAY(4),
    SATURDAY(5),
    SUNDAY(6);

    private final int number;

    Weekday(int number) {
        this.number = number;
    }

    public static Weekday fromInt(int number) {
        for (Weekday frequency : values()) {
            if (frequency.number == number) {
                return frequency;
            }
        }
        throw new InvalidParameterException("Provided parameter " + number + " is invalid");
    }

    public int getNumber() {
        return number;
    }
}
