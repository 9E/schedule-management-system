package com.example.schedulemanagementsystem.entity;

import java.security.InvalidParameterException;

public enum Frequency {
    WEEKLY(2),
    DAILY(3);

    private final int number;

    Frequency(int number) {
        this.number = number;
    }

    public static Frequency fromInt(int number) {
        for (Frequency frequency : values()) {
            if (frequency.number == number) {
                return frequency;
            }
        }
        throw new InvalidParameterException("Provided parameter " + number + " is invalid");
    }

    public int getNumber() {
        return number;
    }
}
