package com.example.schedulemanagementsystem.entity;

import com.vladmihalcea.hibernate.type.array.ListArrayType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.List;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@TypeDefs(@TypeDef(name = "enum-array", typeClass = ListArrayType.class,
        parameters = @Parameter(name = ListArrayType.SQL_ARRAY_TYPE, value = "varchar")))
public class RRule {
    @Enumerated(EnumType.STRING)
    private Frequency frequency;
    private Integer interval;
    @Type(type = "enum-array")
    @Column(columnDefinition = "varchar(9)[]")
    private List<Weekday> weekdays;
    private ZonedDateTime dateStart;
    private ZonedDateTime dateUntil;

    public RRule(RRule rRule) {
        frequency = rRule.frequency;
        interval = rRule.interval;
        dateStart = rRule.dateStart;
        dateUntil = rRule.dateUntil;
    }
}
