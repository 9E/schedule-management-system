package com.example.schedulemanagementsystem.service;

import com.example.schedulemanagementsystem.entity.Event;
import com.example.schedulemanagementsystem.entity.User;
import com.example.schedulemanagementsystem.exception.EmptyCustomDaysException;
import com.example.schedulemanagementsystem.exception.IncorrectCustomDaysSizeException;
import com.example.schedulemanagementsystem.exception.RecordNotFoundException;
import com.example.schedulemanagementsystem.exception.WrongCreateEventMethodException;
import com.example.schedulemanagementsystem.repository.EventRepository;
import com.example.schedulemanagementsystem.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
@Transactional
public class EventService {
    private final UserService userService;
    private final UserRepository userRepository;
    private final EventRepository eventRepository;

    public Event findById(Long eventId) {
        return eventRepository.findById(eventId)
                .orElseThrow(() -> new RecordNotFoundException("Event with id " + eventId + "was not found"));
    }

    public List<Event> findByIdProcessed(Long eventId) {
        return buildCustomDayEvents(findById(eventId));
    }

    public List<Event> findByUserIdProcessed(Long userId) {
        List<Event> events = eventRepository.findByUserId(userId);
        List<Event> nonDailyCustomEvents = getNonDailyCustomEvents(events);
        List<Event> dailyCustomEvents = buildCustomDayEvents(events);

        return Stream.of(nonDailyCustomEvents, dailyCustomEvents)
                .flatMap(Collection::stream)
                .toList();
    }

    public List<Event> findByUserId (Long userId) {
        return eventRepository.findByUserId(userId);
    }

    private List<Event> getNonDailyCustomEvents(List<Event> events) {
        return events.stream()
                .filter(event -> !event.isDailyCustomFrequency())
                .toList();
    }

    private List<Event> buildCustomDayEvents(List<Event> events) {
        return events.stream()
                .filter(Event::isDailyCustomFrequency)
                .map(this::buildCustomDayEvents)
                .flatMap(Collection::stream)
                .toList();
    }

    private List<Event> buildCustomDayEvents(Event event) {
        return IntStream.range(0, event.getCustomDays().size())
                .filter(index -> event.getCustomDays().get(index))
                .mapToObj(index -> {
                    Event builtEvent = new Event(event);
                    ZonedDateTime newDate = builtEvent.getRRule().getDateStart().plusDays(index);
                    builtEvent.getRRule().setDateStart(newDate);
                    return builtEvent;
                }).toList();
    }

    public Event createEvent(Long userId, Event event) {
        if (event.isDailyCustomFrequency()) {
            throw new WrongCreateEventMethodException("This method doesn't support DAILY_CUSTOM frequency.");
        }

        User user = userService.findById(userId);
        event.setUser(user);
        user.getEvents().add(event);

        userRepository.save(user);
        return eventRepository.save(event);
    }

    public List<Event> createCustomDaysEvents(Long userId, Event event) {
        if (!event.isDailyCustomFrequency()) {
            throw new WrongCreateEventMethodException("This method supports only DAILY_CUSTOM frequency.");
        }
        validateEventForCustomDays(event);
        User user = userService.findById(userId);

        event.setUser(user);
        user.getEvents().add(event);
        userRepository.save(user);
        return buildCustomDayEvents(eventRepository.save(event));
    }

    public Event updateEvent(Long eventId, Event eventData) {
        if (eventData.isDailyCustomFrequency()) {
            validateEventForCustomDays(eventData);
        }

        Event event = findById(eventId);
        event.setTitle(eventData.getTitle());
        event.setAllDay(eventData.getAllDay());
        event.setRRule(eventData.getRRule());
        event.setDuration(eventData.getDuration());
        event.setDailyCustomFrequency(eventData.isDailyCustomFrequency());
        event.setCustomDays(eventData.getCustomDays());
        return event;
    }

    private void validateEventForCustomDays(Event event) {
        if (event.getCustomDays().isEmpty()) {
            throw new EmptyCustomDaysException("Custom days array can't be empty.");
        }
        if (event.getRRule().getInterval() != event.getCustomDays().size()) {
            throw new IncorrectCustomDaysSizeException("Interval and custom days size muse be equal.");
        }
    }

    public void deleteById(Long id) {
        Event event = findById(id);
        eventRepository.delete(event);
    }
}
