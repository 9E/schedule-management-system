package com.example.schedulemanagementsystem.service;

import com.example.schedulemanagementsystem.entity.User;
import com.example.schedulemanagementsystem.exception.BadCredentialsException;
import com.example.schedulemanagementsystem.exception.RecordNotFoundException;
import com.example.schedulemanagementsystem.exception.UsernameTakenException;
import com.example.schedulemanagementsystem.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    public User findById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException("User with id " + id + " was not found"));
    }

    public User save(User user) {
        validateUser(user);

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    public User update(Long id, User userData) {
        User user = findById(id);
        if (!Objects.equals(user.getUsername(), userData.getUsername())) {
            validateUser(userData);
        }
        user.setUsername(userData.getUsername());
        user.setPassword(passwordEncoder.encode(userData.getPassword()));
        user.setEmail(userData.getEmail());
        return userRepository.save(user);
    }

    private void validateUser(User user) {
        if (userRepository.existsByUsername(user.getUsername())) {
            throw new UsernameTakenException("User with such username already exists");
        }
    }

    public User login(User user) {
        String username = user.getUsername();
        User userInDatabase = userRepository.findByUsername(username)
                .orElseThrow(() -> new BadCredentialsException("Bad credentials while logging in."));

        if (!passwordEncoder.matches(user.getPassword(), userInDatabase.getPassword())) {
            throw new BadCredentialsException("Bad credentials while logging in.");
        }
        return userInDatabase;
    }
}
